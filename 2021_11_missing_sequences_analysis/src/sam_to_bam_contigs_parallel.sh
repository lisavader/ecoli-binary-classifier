#!/bin/bash

#1. mkdir for holding the scripts
mkdir sam_to_bam_jobs_contigs

#2. mkdir for results
mkdir ../results/bam_files_contigs
mkdir ../results/bam_files_sorted_contigs

#3. Get a list of strains
list_strains=$(cat ../results/missing_plasmids.csv)

#4. Create files
for strains in $list_strains
do
echo "#!/bin/bash
source /home/dla_mm/jpaganini/data/miniconda3/etc/profile.d/conda.sh
conda activate samtools_mmbioit
samtools view -S -b ../../results/sam_files_contigs/${strains}.sam > ../../results/bam_files_contigs/${strains}.bam
samtools sort -o ../../results/bam_files_sorted_contigs/${strains}_sorted.bam ../../results/bam_files_contigs/${strains}.bam" > sam_to_bam_jobs_contigs/${strains}.sh
done

#5. Run the scripts
cd sam_to_bam_jobs_contigs
jobs=$(ls)
for slurm in $jobs
do
sbatch --time=2:00:00 --mem=8G ${slurm} -c 8
done

