#!/bin/bash

#1. mkdir for holding the scripts
mkdir sam_to_bam_jobs

#2. mkdir for results
mkdir ../results/bam_files
mkdir ../results/bam_files_sorted

#3. Get a list of strains
list_strains=$(cat ../../2021_08_prepare_dataset/results/benchmark_strains.csv | sed 's/"//g')

#4. Create files
for strains in $list_strains
do
echo "#!/bin/bash
source /home/dla_mm/jpaganini/data/miniconda3/etc/profile.d/conda.sh
conda activate samtools_mmbioit
samtools view -S -b ../../results/sam_files/${strains}.sam > ../../results/bam_files/${strains}.bam
samtools sort -o ../../results/bam_files_sorted/${strains}_sorted.bam ../../results/bam_files/${strains}.bam" > sam_to_bam_jobs/${strains}.sh
done

#5. Run the scripts
cd sam_to_bam_jobs
jobs=$(ls)
for slurm in $jobs
do
sbatch --time=2:00:00 --mem=8G ${slurm} -c 8
done

