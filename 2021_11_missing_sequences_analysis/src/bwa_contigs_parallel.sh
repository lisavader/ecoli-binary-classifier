#!/bin/bash

#1. Get a list of all benchmark genomes
list_strains=$(cat ../results/missing_plasmids.csv)

#2. Create a directory for bwa jobs
mkdir bwa_jobs_contigs

#3. create directory for holding sam files
mkdir ../results/sam_files_contigs

#4. Index reference genomes and align reads
for strains in $list_strains
do
echo "#!/bin/bash
source /home/dla_mm/jpaganini/data/miniconda3/etc/profile.d/conda.sh
conda activate bwa_mmbioit
#bwa index ../../../2021_08_prepare_dataset/data/reference_genomes/${strains}_genomic.fna
bwa mem -a ../../../2021_08_prepare_dataset/data/reference_genomes/${strains}_genomic.fna ../../../2021_09_gplas_integration/plasmidEC/ecoli_nodes_500bp//${strains}.fasta > ../../results/sam_files_contigs/${strains}.sam" > bwa_jobs_contigs/${strains}.sh
done

#5. Run the scripts
cd bwa_jobs_contigs
jobs=$(ls)
for slurm in $jobs
do
sbatch --time=2:00:00 --mem=8G ${slurm} -c 8
done



