#!/bin/bash

#1. Get a list of strains
files=$(cat ../../2021_08_prepare_dataset/results/benchmark_strains.csv | sed 's/"//g')

#2. Move to the right directory
cd ../gplas/gplas_input

#3. Loop thru each of the files and extract info
for strain in $files
do
grep '>' ${strain}_raw_nodes.fasta | while read line
do
node_number=$(echo $line | cut -f 1 -d _ | sed 's/>S//g')
node_length=$(echo $line | cut -f 3 -d : | sed 's/_dp//g')
node_coverage=$(echo $line | cut -f 5 -d :)
echo -e ${strain},${node_number},${node_length},${node_coverage} >> ../../results/all_nodes_length_coverage.csv
done
done
