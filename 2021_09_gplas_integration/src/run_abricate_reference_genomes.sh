#!/bin/bash

#Pass the miniconda3 installation directory with the -c flag.

source /home/dla_mm/jpaganini/data/miniconda3/etc/profile.d/conda.sh
conda activate abricate_mmbioit

#1. Create a file that contains a lits of all the files for which we will run the Abricate
cd ../../2021_08_prepare_dataset/data/reference_genomes

find $(pwd) -type f >> ../../../../2021_09_gplas_integration/data/abricate_reference_genomes_paths.txt

#2. Run abricate
abricate --fofn ../../../../2021_09_gplas_integration/data/abricate_reference_genomes_paths.txt --csv > ../../../../2021_09_gplas_integration/results/abricate_output_reference_genomes.csv 
