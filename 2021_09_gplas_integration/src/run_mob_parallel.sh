#!/bin/bash

###Predictions for 1kb contigs
mkdir mob_slurm_jobs_1kb
mkdir mob_slurm_jobs_500bp
mkdir ../results/mob_predictions_1kb
mkdir ../results/mob_predictions_500bp

files=$(cat ../../2021_08_prepare_dataset/results/benchmark_strains.csv | sed 's/"//g')

#create slurm scripts
for strains in $files
do
echo "#!/bin/bash
source /home/dla_mm/jpaganini/data/miniconda3/etc/profile.d/conda.sh
conda activate mob-suite_mmbioit
cd ../../results/mob_predictions_1kb
mob_recon --infile ../../plasmidEC/ecoli_nodes_1kb/${strains}.fasta --outdir ${strains}" > mob_slurm_jobs_1kb/${strains}.sh
done

echo "#!/bin/bash
source /home/dla_mm/jpaganini/data/miniconda3/etc/profile.d/conda.sh
conda activate mob-suite_mmbioit
cd ../../results/mob_predictions_500bp
mob_recon --infile ../../plasmidEC/ecoli_nodes_500bp/${strains}.fasta --outdir ${strains}" > mob_slurm_jobs_500bp/${strains}.sh

#Run the scripts
cd mob_slurm_jobs_1kb
jobs=$(ls)
for slurm in $jobs
do
sbatch --time=4:00:00 --mem=8G ${slurm} -c 8
done

cd ../mob_slurm_jobs_500bp
jobs_500=$(ls)
for slurm in $jobs_500
do
sbatch --time=4:00:00 --mem=8G ${slurm} -c 8
done



###Predictions for 500bp
