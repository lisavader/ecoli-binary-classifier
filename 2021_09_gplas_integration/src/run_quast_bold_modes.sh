#!/bin/bash

#1. create directory to hold the results
mkdir ../results/quast_output

source /home/dla_mm/jpaganini/data/miniconda3/etc/profile.d/conda.sh

conda activate quast_mmbioit

##----------run quast for gplas predictions with different bold parameters --------------------#

#get a list of files
files=$(cat ../data/unbinned_training.csv)

#get a list of directories where the gplas results are
gplas_results=$(ls -1d ../gplas/results/testing_bold_modes/plasmidec_bold* |xargs -n 1 basename)

#loop thru the directories and run quast
for directory in $gplas_results
do
#create directory to hold the quast results
mkdir -p ../results/quast_output/${directory}
#run quast, the target is aligning each individual prediction to the complete reference genome to then extract the data
for strains in $files
do
all_bins=$(ls ../gplas/results/testing_bold_modes/${directory}/${strains}_bin_*_final.fasta | cut -f 6 -d / | sed 's/_final.fasta//g')
for bin in $all_bins
do
#echo ${bin}
quast -o ../results/quast_output/${directory}/${strains}/${bin} -r ../../2021_08_prepare_dataset/data/reference_genomes/${strains}_genomic.fna -m 1000 -t 8 -i 300 --no-snps --ambiguity-usage all ../gplas/results/${directory}/${bin}_final.fasta
done
done
done
