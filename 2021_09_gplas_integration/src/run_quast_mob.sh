#!/bin/bash

#1. get command line optoins
while getopts :s:p:r:o: flag; do
	case $flag in
		s) strain_file=$OPTARG;;
		p) predictions_directory=$OPTARG;;
		r) reference_directory=$OPTARG;;
        o) output_directory=$OPTARG;;
	esac
done

#2. activate conda environment

source /home/dla_mm/jpaganini/data/miniconda3/etc/profile.d/conda.sh
conda activate quast_mmbioit

#3. Clean paths given by user
predictions_directory=$(echo ${predictions_directory} | sed 's#/*$##g')
reference_directory=$(echo ${reference_directory} | sed 's#/*$##g')
output_directory=$(echo ${output_directory} | sed 's#/*$##g')

#4. get a list of files
files=$(cat ${strain_file} | sed 's/"//g') 

#make directory to hold results
mkdir -p ${output_directory}

#run quast, the target is aligning each individual prediction to the complete reference genome to then extract the data
for strains in $files
do
all_bins=$(ls ${predictions_directory}/${strains}/plasmid*.fasta | sed "s#${predictions_directory}/${strains}/##g" | sed 's/.fasta//g')
for bin in $all_bins
do
quast -o ${output_directory}/${strains}/${bin} -r ${reference_directory}/${strains}_genomic.fna -m 1000 -t 8 -i 300 --no-snps --ambiguity-usage all ${predictions_directory}/${strains}/${bin}.fasta
done
done
