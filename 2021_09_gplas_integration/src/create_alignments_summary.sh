#!/bin/bash

source /home/dla_mm/jpaganini/data/miniconda3/etc/profile.d/conda.sh
conda activate python_codes_mmbioit

#gather results from self-alignments
python gather_quast_results.py ../plasmidEC/ecoli_nodes_1kb ../results/quast_output/self_alignments_gfa_1kb
python gather_quast_results.py ../plasmidEC/ecoli_nodes_500bp ../results/quast_output/self_alignments_gfa_500 

#gather results for gplas_plasmidec original version
python gather_quast_results.py ../gplas/results/plasmidec_1kb_original ../results/quast_output/plasmidec_1kb_original

#get the results for testing the different bold modes
python gather_quast_results.py ../gplas/results/testing_bold_modes/plasmidec_bold_05 ../results/quast_output/plasmidec_bold_05
python gather_quast_results.py ../gplas/results/testing_bold_modes/plasmidec_bold_10 ../results/quast_output/plasmidec_bold10
python gather_quast_results.py ../gplas/results/testing_bold_modes/plasmidec_bold_15 ../results/quast_output/plasmidec_bold15
python gather_quast_results.py ../gplas/results/testing_bold_modes/plasmidec_bold_20 ../results/quast_output/plasmidec_bold20

#gather results for plascope_gplas
python gather_quast_results.py ../gplas/results/final_benchmark/plascope_1kb ../results/quast_output/plascope_1kb
python gather_quast_results.py ../gplas/results/final_benchmark/plascope_500bp ../results/quast_output/plascope_500bp

#gather results for MOB-suite
python gather_quast_results.py ../results/mob_predictions_1kb ../results/quast_output/mob_1kb
python gather_quast_results.py ../results/mob_predictions_500 ../results/quast_output/mob_500

