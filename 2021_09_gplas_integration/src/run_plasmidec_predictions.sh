#!/bin/bash

#1. Move to the directory of plasmidEC
mv ../plasmidEC

#2. Login to a computing node
srun --mem 30G --time 48:00:00 --gres=tmpspace:200G -c 8 --pty bash

#3. Run plasmidEC
bash ensemble_classifier.sh -i ecoli_nodes_1kb -o ecoli_predictions_1kb -g
bash ensemble_classifier.sh -i ecoli_nodes_500bp -o ecoli_predictions_500bp -g
