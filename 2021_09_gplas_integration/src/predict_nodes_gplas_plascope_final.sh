#!/bin/bash

source ~/data/miniconda3/etc/profile.d/conda.sh

#activate proper conda env
conda activate gplas_plasmidec_test

#get a list of strains
strains=$(cat ../../2021_08_prepare_dataset/results/benchmark_strains.csv | sed 's/"//g')

#cp predictions 1kb
cp ../results/plascope_predictions/plascope_1kb/* ../gplas/independent_prediction

#move to gplas directory
cd ../gplas

#loop thru the graphs
for genome in $strains
do
./gplas.sh -i ../../2021_08_prepare_dataset/results/benchmark_ecoli_graphs/${genome}.gfa -c predict -s 'Escherichia coli' -x 50 -n ${genome} -b 5
done

#move results
mkdir results/final_benchmark/plascope_1kb
mv results/GCA* results/final_benchmark/plascope_1kb

#remove predictions
rm independent_predictions/GCA*

#move 500bp predictions into the independent_predictions directory
cp ../results/plascope_predictions/plascope_500bp/* independent_prediction/

#predict with 500 bp data
for genome in $strains
do
./gplas.sh -i ../../2021_08_prepare_dataset/results/benchmark_ecoli_graphs/${genome}.gfa -c predict -s 'Escherichia coli' -x 50 -n ${genome} -b 5
done

#move the results
mkdir results/final_benchmark/plascope_500bp
mv results/GCA* results/plascope_predictions/plascope_500bp

#remove the predictions
rm independent_predictions/GCA*
