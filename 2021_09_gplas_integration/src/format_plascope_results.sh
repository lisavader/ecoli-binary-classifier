#!/bin/bash

#1. Get a list of strains
strains=$(cat 2021_08_prepare_dataset/results/benchmark_strains.csv | sed 's/"//g')

#2. Make directory to store Plascope results
mkdir ../results/plascope_predictions
mkdir ../results/plascope_predictions/plascope_1kb
mkdir ../results/plascope_predictions/plascope_500bp

#3. Loop thru plascope predictions and extract results 1-kb
for genome in $strains
do
echo -e '"Prob_Chromosome"\t"Prob_Plasmid"\t"Prediction"\t"Contig_name"\t"Contig_length"' > ../results/plascope_predictions/plascope_1kb/${genome}_plasmid_prediction.tab
cat ../plasmidEC/ecoli_predictions_1kb/plascope_predictions/${genome}_PlaScope/Centrifuge_results/${genome}_list | while read line
do
classification=$(echo $line | cut -f 2 -d ' ')
contig=$(echo $line | cut -f 1 -d ' ')
length=$(echo $contig | cut -f 3 -d : | cut -f 1 -d _ )
if [ $classification = 'plasmid' ]
then
echo -e 0'\t'1'\t''"Plasmid"''\t''"'$contig'"''\t'$length >> ../results/plascope_predictions/plascope_1kb/${genome}_plasmid_prediction.tab
elif [ $classification = 'chromosome' ]
then
echo -e 1'\t'0'\t''"Chromosome"''\t''"'$contig'"''\t'$length >> ../results/plascope_predictions/plascope_1kb/${genome}_plasmid_prediction.tab
elif [ $classification = 'unclassified' ]
then
echo -e 0.5'\t'0.5'\t''"Plasmid"''\t''"'$contig'"''\t'$length >> ../results/plascope_predictions/plascope_1kb/${genome}_plasmid_prediction.tab
fi
done
done

#3. Loop thru plascope predictoins 500 bp and extract results
for genome in $strains
do
echo -e '"Prob_Chromosome"\t"Prob_Plasmid"\t"Prediction"\t"Contig_name"\t"Contig_length"' > ../results/plascope_predictions/plascope_500bp/${genome}_plasmid_prediction.tab
cat ../plasmidEC/ecoli_predictions_500bb/plascope_predictions/${genome}_PlaScope/Centrifuge_results/${genome}_list | while read line
do
classification=$(echo $line | cut -f 2 -d ' ')
contig=$(echo $line | cut -f 1 -d ' ')
length=$(echo $contig | cut -f 3 -d : | cut -f 1 -d _ )
if [ $classification = 'plasmid' ]
then
echo -e 0'\t'1'\t''"Plasmid"''\t''"'$contig'"''\t'$length >> ../results/plascope_predictions/plascope_500bp/${genome}_plasmid_prediction.tab
elif [ $classification = 'chromosome' ]
then
echo -e 1'\t'0'\t''"Chromosome"''\t''"'$contig'"''\t'$length >> ../results/plascope_predictions/plascope_500bp/${genome}_plasmid_prediction.tab
elif [ $classification = 'unclassified' ]
then
echo -e 0.5'\t'0.5'\t''"Plasmid"''\t''"'$contig'"''\t'$length >> ../results/plascope_predictions/plascope_500bp/${genome}_plasmid_prediction.tab
fi
done
done
