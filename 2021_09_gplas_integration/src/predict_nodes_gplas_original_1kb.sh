#!/bin/bash

source ~/data/miniconda3/etc/profile.d/conda.sh

#activate proper conda env
conda activate gplas_plasmidec_test

#get a list of strains
strains=$(cat ../../2021_08_prepare_dataset/results/benchmark_strains.csv | sed 's/"//g')

#move predictions into the independent_predictions directory
cp ../plasmidEC/ecoli_predictions_1kb/* ../gplas/independent_prediction

#move to gplas directory
cd ../gplas

#loop thru the graphs
for genome in $strains
do
./gplas.sh -i ../../2021_08_prepare_dataset/results/benchmark_ecoli_graphs/${genome}.gfa -c predict -s 'Escherichia coli' -x 50 -n ${genome} -b 1
done

#move results
mkdir results/plasmidec_1kb_original
mv results/GCA* results/plasmidec_1kb_original
