#!/bin/bash

#1. Make directories to hold the databases of the binary classifiers
mkdir ../data/databases
mkdir ../data/databases/plascope
mkdir ../data/databases/platon
mkdir ../data/databases/rfplasmid

#2. Download the databases 
#2.1 rfplasmid
wget http://klif.uu.nl/download/plasmid_db/trainingsets2/ncbiftp >> ../data/databases/rfplasmid/ncbiftp.tsv
#2.2 platon
wget https://zenodo.org/record/4066768/files/db.tar.gz >> ../data/databases/platon/db.tar.gz
#decompress platon database
tar -xzf ../data/databases/platon/db.tar.gz
#2.3 Plascope
#the plascope database was obtained from supplementary materials of the tool. See https://pubmed.ncbi.nlm.nih.gov/30265232/
#this table (in pdf format) was manually copied and coverted to csv file.
