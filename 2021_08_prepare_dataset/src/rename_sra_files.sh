#!/bin/bash

#1.move to the directory that contains the reads
cd ../data/sra_files
#2. get a list of sra_codes
sra_codes=$(ls SRR* | cut -f 1 -d _ | sort -u)

#2. Move to the results directories, which contains the file that relates the strains_id with the sra_codes
cd ../../results

for codes in $sra_codes; 
do 
strain=$(grep ${codes} id_sra_list.csv | cut -f 1 -d ,)
mv ../data/sra_files/${codes}_1.fastq ../data/sra_files/${strain}_R1.fastq
mv ../data/sra_files/${codes}_2.fastq ../data/sra_files/${strain}_R2.fastq
done
