#!/bin/bash

#1- Get the list of the biosamples Id to check if we have the reads that will later be used to run the alignments

biosamples=$(tail -n+2 ../data/paganini_2021_benchmark_strains_metadata.csv | cut -f 3 -d , | sed 's/"//g')

#2. Now, we will make a look to get the reads identifiers

for biosample in $biosamples
do
sra_accession=$(esearch -db biosample -query "${biosample}" | elink -target sra | efetch -format runinfo  | grep 'Illumina\|ILLUMINA' | cut -f 1 -d ,)
echo ${biosample},${sra_accession} >> ../results/sra_accessions_list.csv
done



