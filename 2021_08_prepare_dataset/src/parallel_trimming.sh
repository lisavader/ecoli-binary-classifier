#!/bin/bash

#1. get command line optoins
while getopts :s:i:o: flag; do
        case $flag in
                s) strain_file=$OPTARG;; #file with list of strain names
                i) raw_reads=$OPTARG;; #path to directory that contains the raaw reads
        o) output_directory=$OPTARG;; #path to output directory. It will be create if it doesn't exits
        esac
done

#2. Clean paths given by user
raw_reads=$(echo ${raw_reads} | sed 's#/*$##g')
output_directory=$(echo ${output_directory} | sed 's#/*$##g')

#1. create directory to hold the results
mkdir -p ${output_directory}

#get a list of the files
files=$(cat ${strain_file} | sed 's/"//g') 

#2. create folder for temporary slurm scripts
mkdir trim_slurm_jobs

#create slurm scripts
for strains in $files
do
echo "#!/bin/bash
source /home/dla_mm/jpaganini/data/miniconda3/etc/profile.d/conda.sh
conda activate trimming_mmbioit
cd ${raw_reads}
trim_galore --quality 20 --dont_gzip --length 20 --paired -j 8 --output_dir ${output_directory} ${strains}*fastq.gz" > trim_slurm_jobs/${strains}.sh
done

#Run the scripts
cd trim_slurm_jobs
jobs=$(ls)
for slurm in $jobs
do
sbatch --time=2:30:00 --mem=5G ${slurm}
done
